tabvariant_calling = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	selectInput("selectvariant_calling", label = "Select the tool to use : ", selected = "gatk_haplotype_caller", choices = list("gatk_haplotype_caller" = "gatk_haplotype_caller", "bcftools_mpileup_and_call" = "bcftools_mpileup_and_call", "deep_variant" = "deep_variant", "freebayes" = "freebayes")),

conditionalPanel(condition = "input.selectvariant_calling == 'gatk_haplotype_caller'",box(title = "GATK Haplotype Caller", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("variant_calling__gatk_haplotype_caller_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

	box(title = "Path to reference genome fasta file", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		selectInput("variant_calling__gatk_haplotype_caller_genome_fasta_select",label = "Select where to find the file", selected = "server", choices = c("On server" = "server", "On your machine" = "local")),
		conditionalPanel(condition = "input.variant_calling__gatk_haplotype_caller_genome_fasta_select == 'server'",
				tags$label("Path to reference genome fasta file"),
				fluidRow(
					column(4,shinyFilesButton("shinyfiles_variant_calling__gatk_haplotype_caller_genome_fasta",label="Please select a file", title="Path to reference genome fasta file", multiple=FALSE)),
					column(8,textInput("variant_calling__gatk_haplotype_caller_genome_fasta_server",label=NULL,value=""))
				)
			),
		conditionalPanel(condition = "input.variant_calling__gatk_haplotype_caller_genome_fasta_select == 'local'",
			fileInput("variant_calling__gatk_haplotype_caller_genome_fasta_local",label = "Path to reference genome fasta file")
		)
	)
,

		p("GATK Haplotype Caller: The HaplotypeCaller is capable of calling SNPs and indels simultaneously via local de-novo assembly of haplotypes in an active region"),

		p("Website : ",a(href="https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_haplotypecaller_HaplotypeCaller.php","https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_haplotypecaller_HaplotypeCaller.php",target="_blank")),

		p("Documentation : ",a(href="https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_haplotypecaller_HaplotypeCaller.php","https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_haplotypecaller_HaplotypeCaller.php",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1101/gr.107524.110","https://doi.org/10.1101/gr.107524.110",target="_blank"))

	)),
conditionalPanel(condition = "input.selectvariant_calling == 'bcftools_mpileup_and_call'",box(title = "BCFtools mpileup and call", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("variant_calling__bcftools_mpileup_and_call_threads", label = "Threads to use", min = 1, max = 32, step = 1, width =  "auto", value = 4),

	box(title = "Path to reference genome fasta file", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		selectInput("variant_calling__bcftools_mpileup_and_call_genome_fasta_select",label = "Select where to find the file", selected = "server", choices = c("On server" = "server", "On your machine" = "local")),
		conditionalPanel(condition = "input.variant_calling__bcftools_mpileup_and_call_genome_fasta_select == 'server'",
				tags$label("Path to reference genome fasta file"),
				fluidRow(
					column(4,shinyFilesButton("shinyfiles_variant_calling__bcftools_mpileup_and_call_genome_fasta",label="Please select a file", title="Path to reference genome fasta file", multiple=FALSE)),
					column(8,textInput("variant_calling__bcftools_mpileup_and_call_genome_fasta_server",label=NULL,value=""))
				)
			),
		conditionalPanel(condition = "input.variant_calling__bcftools_mpileup_and_call_genome_fasta_select == 'local'",
			fileInput("variant_calling__bcftools_mpileup_and_call_genome_fasta_local",label = "Path to reference genome fasta file")
		)
	)
,

		p("BCFtools mpileup and call: BCFtools is a set of utilities that manipulate variant calls in the Variant Call Format (VCF) and its binary counterpart BCF."),

		p("Website : ",a(href="https://samtools.github.io/bcftools/","https://samtools.github.io/bcftools/",target="_blank")),

		p("Documentation : ",a(href="https://samtools.github.io/bcftools/howtos/index.html","https://samtools.github.io/bcftools/howtos/index.html",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1093/bioinformatics/btr509","https://doi.org/10.1093/bioinformatics/btr509",target="_blank"))

	)),
conditionalPanel(condition = "input.selectvariant_calling == 'deep_variant'",box(title = "DeepVariant", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
	box(title = "Path to reference genome fasta file", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		selectInput("variant_calling__deep_variant_genome_fasta_select",label = "Select where to find the file", selected = "server", choices = c("On server" = "server", "On your machine" = "local")),
		conditionalPanel(condition = "input.variant_calling__deep_variant_genome_fasta_select == 'server'",
				tags$label("Path to reference genome fasta file"),
				fluidRow(
					column(4,shinyFilesButton("shinyfiles_variant_calling__deep_variant_genome_fasta",label="Please select a file", title="Path to reference genome fasta file", multiple=FALSE)),
					column(8,textInput("variant_calling__deep_variant_genome_fasta_server",label=NULL,value=""))
				)
			),
		conditionalPanel(condition = "input.variant_calling__deep_variant_genome_fasta_select == 'local'",
			fileInput("variant_calling__deep_variant_genome_fasta_local",label = "Path to reference genome fasta file")
		)
	)
,

		numericInput("variant_calling__deep_variant_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		radioButtons("variant_calling__deep_variant_model_type", label = "Model type", choices = list("WGS" = "WGS", "WES" = "WES"),  selected = "WGS", width =  "auto"),

		p("DeepVariant: DeepVariant is an analysis pipeline that uses a deep neural network to call genetic variants from next-generation DNA sequencing data."),

		p("Website : ",a(href="https://github.com/google/deepvariant","https://github.com/google/deepvariant",target="_blank")),

		p("Documentation : ",a(href="https://github.com/google/deepvariant/blob/r0.9/docs/README.md","https://github.com/google/deepvariant/blob/r0.9/docs/README.md",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1038/nbt.4235","https://doi.org/10.1038/nbt.4235",target="_blank"))

	)),
conditionalPanel(condition = "input.selectvariant_calling == 'freebayes'",box(title = "freebayes", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
	box(title = "Path to reference genome fasta file", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		selectInput("variant_calling__freebayes_genome_fasta_select",label = "Select where to find the file", selected = "server", choices = c("On server" = "server", "On your machine" = "local")),
		conditionalPanel(condition = "input.variant_calling__freebayes_genome_fasta_select == 'server'",
				tags$label("Path to reference genome fasta file"),
				fluidRow(
					column(4,shinyFilesButton("shinyfiles_variant_calling__freebayes_genome_fasta",label="Please select a file", title="Path to reference genome fasta file", multiple=FALSE)),
					column(8,textInput("variant_calling__freebayes_genome_fasta_server",label=NULL,value=""))
				)
			),
		conditionalPanel(condition = "input.variant_calling__freebayes_genome_fasta_select == 'local'",
			fileInput("variant_calling__freebayes_genome_fasta_local",label = "Path to reference genome fasta file")
		)
	)
,

		p("freebayes: Bayesian haplotype-based genetic polymorphism discovery and genotyping"),

		p("Website : ",a(href="https://github.com/ekg/freebayes","https://github.com/ekg/freebayes",target="_blank")),

		p("Documentation : ",a(href="https://github.com/ekg/freebayes","https://github.com/ekg/freebayes",target="_blank")),

		p("Paper : ",a(href="https://doi.org/arxiv.org/abs/1207.3907","https://doi.org/arxiv.org/abs/1207.3907",target="_blank"))

	))))


