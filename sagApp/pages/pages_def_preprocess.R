tabpreprocess = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	selectInput("selectpreprocess", label = "Select the tool to use : ", selected = "fastp", choices = list("fastp" = "fastp", "null" = "null")),

conditionalPanel(condition = "input.selectpreprocess == 'fastp'",box(title = "fastp", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("preprocess__fastp_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		numericInput("preprocess__fastp_complexity_threshold", label = "The threshold for low complexity filter (0~100)", min = 1, max = NA, step = 1, width =  "auto", value = 30),

		textInput("preprocess__fastp_report_title", label = "fastp report title", value = "fastp report", width = "auto"),

		textInput("preprocess__fastp_adapter_sequence", label = "The adapter for read1. For SE data, if not specified, the adapter will be auto-detected. For PE data, this is used if R1/R2 are found not overlapped.", value = "", width = "auto"),

		textInput("preprocess__fastp_adapter_sequence_R2_PE", label = "the adapter for read2 (PE data only). This is used if R1/R2 are found not overlapped. If not specified, it will be the same as <adapter_sequence>", value = "", width = "auto"),

		numericInput("preprocess__fastp_P", label = "One in (--overrepresentation_sampling) reads will be computed for overrepresentation analysis (1~10000), smaller is slower.", min = 1, max = NA, step = 1, width =  "auto", value = 20),

		checkboxInput("preprocess__fastp_correction_PE", label = "Enable base correction in overlapped regions", value = TRUE),

		checkboxInput("preprocess__fastp_low_complexity_filter", label = "Enable low complexity filter. The complexity is defined as the percentage of base that is different from its next base (base[i] != base[i+1]).", value = TRUE),

		checkboxInput("preprocess__fastp_overrepresentation_analysis", label = "enable overrepresented sequence analysis.", value = TRUE),

		p("fastp: A tool designed to provide fast all-in-one preprocessing for FastQ files."),

		p("Website : ",a(href="https://github.com/OpenGene/fastp","https://github.com/OpenGene/fastp",target="_blank")),

		p("Documentation : ",a(href="https://github.com/OpenGene/fastp","https://github.com/OpenGene/fastp",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1093/bioinformatics/bty560","https://doi.org/10.1093/bioinformatics/bty560",target="_blank"))

	)),
conditionalPanel(condition = "input.selectpreprocess == 'null'",box(title = "null", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		p("null: Skip this step")

	))))


