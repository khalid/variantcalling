from tools import *
from raw_reads import raw_reads
workdir: config['params']['results_dir']
import os
import re
import snakemake.utils
import csv

#############
# Wildcards #
#############

STEPS = config["steps"]
PREPARE_REPORT_OUTPUTS = config["prepare_report_outputs"]
PREPARE_REPORT_SCRIPTS = config["prepare_report_scripts"]
OUTPUTS = config["outputs"]
PARAMS_INFO = config["params_info"]
MULTIQC = config["multiqc"]
config = config["params"]

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def reads():
    inputs = dict()
    if (config["SeOrPe"] == "SE"):
        inputs["read"] = raw_reads['read']
    elif (config["SeOrPe"] == "PE"):
        inputs["read"] = raw_reads['read']
        inputs["read2"] = raw_reads['read2']
    return inputs

def preprocess__fastp_inputs():
    return reads()

def mapping__bwa_mem_PE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_PE.output.R1
        inputs["read2"] =  rules.preprocess__fastp_PE.output.R2
    else:
        return reads()
    return inputs

def mapping__bwa_mem_SE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_SE.output.read
    else:
        return reads()
    return inputs

def mapping__bowtie_PE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_PE.output.R1
        inputs["read2"] =  rules.preprocess__fastp_PE.output.R2
    else:
        return reads()
    return inputs

def mapping__bowtie_SE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_SE.output.read
    else:
        return reads()
    return inputs

def mark_duplicates__Picard_MarkDuplicates_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bwa_mem_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bowtie_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bowtie_SE.output.bam
    return inputs

def prepare_fasta__gatk_prepare_fasta_inputs():
    inputs = dict()
    inputs["fasta"] = config["genome_fasta"]
    return inputs

def indel_realign__gatk_IndelRealigner_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bwa_mem_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bowtie_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bowtie_SE.output.bam
    if (config["mark_duplicates"] == "Picard_MarkDuplicates"):
        inputs["bam"] = rules.mark_duplicates__Picard_MarkDuplicates.output.sorted_bam
    inputs["genome_fasta"] = rules.prepare_fasta__gatk_prepare_fasta.output.fasta
    return inputs

def variant_calling__gatk_haplotype_caller_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bwa_mem_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bowtie_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bowtie_SE.output.bam
    if (config["indel_realign"] == "gatk_IndelRealigner"):
        inputs["bam"] = rules.indel_realign__gatk_IndelRealigner.output.sorted_bam
    elif (config["mark_duplicates"] == "Picard_MarkDuplicates"):
        inputs["bam"] = rules.mark_duplicates__Picard_MarkDuplicates.output.sorted_bam
    inputs["genome_fasta"] = rules.prepare_fasta__gatk_prepare_fasta.output.fasta
    return inputs

def variant_calling__bcftools_mpileup_and_call_inputs():
    return variant_calling__gatk_haplotype_caller_inputs()

def variant_calling__deep_variant_inputs():
    return variant_calling__gatk_haplotype_caller_inputs()

def variant_calling__freebayes_inputs():
    return variant_calling__gatk_haplotype_caller_inputs()

def prepare_report_inputs():
    inputs = list()
    for step in STEPS:
        inputs.extend(step_outputs(step["name"]))
    return inputs

def prepare_report_scripts():
    scripts = list()
    for step in STEPS:
        tool = config[step["name"]]
        prefix = tool+".prepare.report."
        if type(PREPARE_REPORT_SCRIPTS) == type(""):
            if prefix in PREPARE_REPORT_SCRIPTS:
                scripts.append("/workflow/scripts/"+PREPARE_REPORT_SCRIPTS)
        else :        
            script = [s for s in PREPARE_REPORT_SCRIPTS if prefix in s]
            if (len(script)==1):
                scripts.append("/workflow/scripts/"+script[0])
    return scripts

def prepare_report_outputs():
    outputs = list()
    outputs.append(config["results_dir"] + "/outputs_mqc.csv")
    for step in STEPS:
        tool = config[step["name"]]
        if (tool in PREPARE_REPORT_OUTPUTS.keys()):
            if type(PREPARE_REPORT_OUTPUTS[tool]) == type(""):
                outputs.append(config["results_dir"]+"/"+tool+"/"+PREPARE_REPORT_OUTPUTS[tool])
            else:
                for output in PREPARE_REPORT_OUTPUTS[tool]:
                    outputs.append(config["results_dir"]+"/"+tool+"/"+output)
    return outputs

def multiqc_inputs():
    # Need prepare_report inputs and outputs in case prepare_reports has no outputs
    return prepare_report_outputs()

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "preprocess"):
        if (config[step] == "fastp"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.preprocess__fastp_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.preprocess__fastp_SE.output,sample=SAMPLES)

    elif (step == "mapping"):
        if (config[step] == "bwa"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.mapping__bwa_mem_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.mapping__bwa_mem_SE.output,sample=SAMPLES)
        elif (config[step] == "bowtie"):
            if (config["SeOrPe"] == "PE"):
                outputs = expand(rules.mapping__bowtie_PE.output.bam,sample=SAMPLES)
            else:
                outputs = expand(rules.mapping__bowtie_SE.output.bam,sample=SAMPLES)

    elif (step == "mark_duplicates"):
        if (config[step] == "Picard_MarkDuplicates"):
            outputs = expand(rules.mark_duplicates__Picard_MarkDuplicates.output,sample=SAMPLES)

    elif (step == "indel_realign"):
        if (config[step] == "gatk_IndelRealigner"):
            outputs = expand(rules.indel_realign__gatk_IndelRealigner.output,sample=SAMPLES)

    elif (step == "variant_calling"):
        if (config[step] == "gatk_haplotype_caller"):
            outputs = expand(rules.variant_calling__gatk_haplotype_caller.output,sample=SAMPLES)
        elif (config[step] == "bcftools_mpileup"):
            outputs = expand(rules.variant_calling__bcftools_mpileup_and_call.output,sample=SAMPLES)
        elif (config[step] == "deep_variant"):
            outputs = expand(rules.variant_calling__deep_variant.output,sample=SAMPLES)
        elif (config[step] == "freebayes"):
            outputs = expand(rules.variant_calling__freebayes.output,sample=SAMPLES)

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########


if config["SeOrPe"] == "PE":

    rule preprocess__fastp_PE:
        input:
            **preprocess__fastp_inputs()
        output:
            report_html = config["results_dir"]+"/"+config["preprocess__fastp_PE_output_dir"]+"/fastp_report_{sample}.html",
            report_json = config["results_dir"]+"/"+config["preprocess__fastp_PE_output_dir"]+"/fastp_report_{sample}.json",
            R1 = config["results_dir"]+"/"+config["preprocess__fastp_PE_output_dir"]+"/{sample}_R1.fq.gz",
            R2 = config["results_dir"]+"/"+config["preprocess__fastp_PE_output_dir"]+"/{sample}_R2.fq.gz"
        params:
            command = config["preprocess__fastp_PE_command"],
            complexity_threshold = config["preprocess__fastp_complexity_threshold"],
            report_title = config["preprocess__fastp_report_title"],
            adapter_sequence = config["preprocess__fastp_adapter_sequence"],
            adapter_sequence_R2 = config["preprocess__fastp_adapter_sequence_R2_PE"],
            P = config["preprocess__fastp_P"],
            output_dir = config["preprocess__fastp_PE_output_dir"],
            correction = "--correction " if config["preprocess__fastp_correction_PE"] == True else "",
            low_complexity_filter = "--low_complexity_filter " if config["preprocess__fastp_low_complexity_filter"] == True else "",
            overrepresentation_analysis = "--overrepresentation_analysis " if config["preprocess__fastp_overrepresentation_analysis"] == True else "",
        log:
            config["results_dir"]+"/logs/" + config["preprocess__fastp_PE_output_dir"] + "/{sample}_fastp_log.txt"
        threads:
            config["preprocess__fastp_threads"]
        shell:
            "{params.command} "
            "-i {input.read} "
            "-I {input.read2} "
            "-o {output.R1} "
            "-O {output.R2} "
            "-w {threads} "
            "{params.correction} "
            "{params.low_complexity_filter} "
            "--complexity_threshold {params.complexity_threshold} "
            "--html {output.report_html} "
            "--json {output.report_json} "
            "--report_title {params.report_title} "
            "--adapter_sequence '{params.adapter_sequence}' "
            "--adapter_sequence_r2 '{params.adapter_sequence_R2}' "
            "{params.overrepresentation_analysis} "
            "-P {params.P} "
            "|& tee {log}"


elif config["SeOrPe"] == "SE":

    rule preprocess__fastp_SE:
        input:
            **preprocess__fastp_inputs()
        output:
            report_html = config["results_dir"]+"/"+config["preprocess__fastp_SE_output_dir"]+"/fastp_report_{sample}.html",
            report_json = config["results_dir"]+"/"+config["preprocess__fastp_SE_output_dir"]+"/fastp_report_{sample}.json",
            read = config["results_dir"]+"/"+config["preprocess__fastp_SE_output_dir"]+"/{sample}.fq.gz",
        params:
            command = config["preprocess__fastp_SE_command"],
            complexity_threshold = config["preprocess__fastp_complexity_threshold"],
            report_title = config["preprocess__fastp_report_title"],
            adapter_sequence = config["preprocess__fastp_adapter_sequence"],
            P = config["preprocess__fastp_P"],
            output_dir = config["preprocess__fastp_SE_output_dir"],
            low_complexity_filter = "--low_complexity_filter " if config["preprocess__fastp_low_complexity_filter"] == True else "",
            overrepresentation_analysis = "--overrepresentation_analysis " if config["preprocess__fastp_overrepresentation_analysis"] == True else "",
        log:
            config["results_dir"]+"/logs/" + config["preprocess__fastp_SE_output_dir"] + "/{sample}_fastp_log.txt"
        threads:
            config["preprocess__fastp_threads"]
        shell:
            "{params.command} "
            "-i {input.read} "
            "-o {output.R1} "
            "-w {threads} "
            "{params.low_complexity_filter} "
            "--complexity_threshold {params.complexity_threshold} "
            "--html {output.report_html} "
            "--json {output.report_json} "
            "--report_title {params.report_title} "
            "--adapter_sequence '{params.adapter_sequence}' "
            "{params.overrepresentation_analysis} "
            "-P {params.P} "
            "|& tee {log}"




rule mapping__bwa_index:
    input:
        genome_fasta = config["genome_fasta"]
    output:
        index = (
            config["mapping__bwa_index_path"]+"/index.amb",
            config["mapping__bwa_index_path"]+"/index.ann",
            config["mapping__bwa_index_path"]+"/index.bwt",
            config["mapping__bwa_index_path"]+"/index.pac",
            config["mapping__bwa_index_path"]+"/index.sa"
        )
    log:
        config["results_dir"]+"/logs/" + config["mapping__bwa_index_output_dir"] + "/index.log"
    params:
        command = config["mapping__bwa_index_command"],
        algorithm = config["mapping__bwa_index_algorithm"],
        output_prefix =  config["mapping__bwa_index_path"]+"/index"
    shell:
        "{params.command} "
        "-p {params.output_prefix} "
        "-a {params.algorithm} "
        "{input.genome_fasta} |& tee {log}"

if config["SeOrPe"] == "PE":

    rule mapping__bwa_mem_PE:
        input:
            **mapping__bwa_mem_PE_inputs(),
            index = (
                config["mapping__bwa_index_path"]+"/index.amb",
                config["mapping__bwa_index_path"]+"/index.ann",
                config["mapping__bwa_index_path"]+"/index.bwt",
                config["mapping__bwa_index_path"]+"/index.pac",
                config["mapping__bwa_index_path"]+"/index.sa"
            )
        output:
            bam = config["results_dir"]+"/"+config["mapping__bwa_mem_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["mapping__bwa_mem_PE_output_dir"] + "/{sample}_bwa_mem_log.txt"
        threads:
            config["mapping__bwa_mem_threads"]
        params:
            command = config["mapping__bwa_mem_PE_command"],
            indexPrefix =  config["mapping__bwa_index_path"]+"/index",
            quality0_multimapping = """| awk '{{pos = index("XA:Z",$0); if (pos>0) $5=0;print}}'""" if (config["mapping__bwa_mem_quality0_multimapping"] == True) else ""
        shell:
            "{params.command} "
            "-t {threads} "
            "{params.indexPrefix} "
            "-R '@RG\\tID:WAW\\tSM:{wildcards.sample}' "
            "{input.read} "
            "{input.read2} 2> {log} "
            "{params.quality0_multimapping} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"

elif config["SeOrPe"] == "SE":

    rule mapping__bwa_mem_SE:
        input:
            **mapping__bwa_mem_SE_inputs(),
            index = (
                config["mapping__bwa_index_path"]+"/index.amb",
                config["mapping__bwa_index_path"]+"/index.ann",
                config["mapping__bwa_index_path"]+"/index.bwt",
                config["mapping__bwa_index_path"]+"/index.pac",
                config["mapping__bwa_index_path"]+"/index.sa"
            )
        output:
            bam = config["results_dir"]+"/"+config["mapping__bwa_mem_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["mapping__bwa_mem_SE_output_dir"] + "/{sample}_bwa_mem_log.txt"
        threads:
            config["mapping__bwa_mem_threads"]
        params:
            command = config["mapping__bwa_mem_SE_command"],
            indexPrefix =  config["mapping__bwa_index_path"]+"/index",
            quality0_multimapping = """| awk '{{pos = index("XA:Z",$0); if (pos>0) $5=0;print}}'""" if (config["mapping__bwa_mem_quality0_multimapping"] == True) else ""
        shell:
            "{params.command} "
            "-t {threads} "
            "{params.indexPrefix} "
            "-R '@RG\\tID:WAW\\tSM:{wildcards.sample}' "
            "{input.read} 2> {log} "
            "{params.quality0_multimapping} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"


rule mapping__bowtie_index:
    input:
        genome_fasta = config["genome_fasta"]
    output:
        index = (
            expand(config["mapping__bowtie_index_path"]+"/index.{num}.ebwt",num=[1,2,3,4]),
            expand(config["mapping__bowtie_index_path"]+"/index.rev.{num}.ebwt",num=[1,2]),
        )
    log:
        config["results_dir"]+"/logs/" + config["mapping__bowtie_index_output_dir"] + "/index.log"
    threads:
        config["mapping__bowtie_index_threads"]
    params:
        command = config["mapping__bowtie_index_command"],
        output_prefix = config["mapping__bowtie_index_path"]+"/index"
    shell:
        "{params.command} "
        "{input.genome_fasta} "
        "{params.output_prefix} "
        "--threads {threads} "
        "|& tee {log}"

if config["SeOrPe"] == "PE":

    rule mapping__bowtie_PE:
        input:
            **mapping__bowtie_PE_inputs(),
            index = (
                expand(config["mapping__bowtie_index_path"]+"/index.{num}.ebwt",num=[1,2,3,4]),
                expand(config["mapping__bowtie_index_path"]+"/index.rev.{num}.ebwt",num=[1,2]),
            )
        output:
            bam = config["results_dir"]+"/"+config["mapping__bowtie_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["mapping__bowtie_PE_output_dir"] + "/{sample}_bowtie_log.txt"
        threads:
            config["mapping__bowtie_threads"]
        params:
            command = config["mapping__bowtie_PE_command"],
            indexPrefix = config["mapping__bowtie_index_path"]+"/index",
            minins = config["mapping__bowtie_minins_PE"],
            maxins = config["mapping__bowtie_maxins_PE"],
            orientation = config["mapping__bowtie_orientation_PE"],
            mult_align_limit = config["mapping__bowtie_mult_align_limit"],
            best = "--best" if (config["mapping__bowtie_best"]) else "",
            strata = "--strata" if (config["mapping__bowtie_strata"]) else ""
        shell:
            "{params.command} "
            "--threads {threads} "
            "{params.indexPrefix} "
            "-I {params.minins} "
            "-X {params.maxins} "
            "{params.orientation} "
            "-m {params.mult_align_limit} "
            "{params.best} "
            "{params.strata} "
            "-S "#output in sam format
            "-1 {input.read} "
            "-2 {input.read2} 2> {log} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"

elif config["SeOrPe"] == "SE":

    rule mapping__bowtie_SE:
        input:
            **mapping__bowtie_SE_inputs(),
            index = (
                expand(config["mapping__bowtie_index_path"]+"/index.{num}.ebwt",num=[1,2,3,4]),
                expand(config["mapping__bowtie_index_path"]+"/index.rev.{num}.ebwt",num=[1,2]),
            )
        output:
            bam = config["results_dir"]+"/"+config["mapping__bowtie_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["mapping__bowtie_SE_output_dir"] + "/{sample}_bowtie_log.txt"
        threads:
            config["mapping__bowtie_threads"]
        params:
            command = config["mapping__bowtie_SE_command"],
            indexPrefix = config["mapping__bowtie_index_path"]+"/index",
            mult_align_limit = config["mapping__bowtie_mult_align_limit"],
            best = "--best" if (config["mapping__bowtie_best"]) else "",
            strata = "--strata" if (config["mapping__bowtie_strata"]) else ""
        shell:
            "{params.command} "
            "--threads {threads} "
            "{params.indexPrefix} "
            "-m {params.mult_align_limit} "
            "{params.best} "
            "{params.strata} "
            "-S "#output in sam format
            "{input.read} 2> {log} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"


rule mark_duplicates__Picard_MarkDuplicates:
    input:
        **mark_duplicates__Picard_MarkDuplicates_inputs(),
    output:
        bam = temp(config["results_dir"]+"/"+config["mark_duplicates__Picard_MarkDuplicates_output_dir"]+"/{sample}.mapped.dedup.bam"),
        sorted_bam = config["results_dir"]+"/"+config["mark_duplicates__Picard_MarkDuplicates_output_dir"]+"/{sample}.mapped.dedup.sorted.bam",
        bai = config["results_dir"]+"/"+config["mark_duplicates__Picard_MarkDuplicates_output_dir"]+"/{sample}.mapped.dedup.sorted.bam.bai",
        metric = config["results_dir"]+"/"+config["mark_duplicates__Picard_MarkDuplicates_output_dir"]+"/duplicate_metrics_{sample}.txt"
    #shadow:
    #    "full" # allow for the automatic removal of temporary files not removed otherwise
    params:
        command = config["mark_duplicates__Picard_MarkDuplicates_command"],
        remove_all_duplicates = config["mark_duplicates__Picard_MarkDuplicates_remove_all_duplicates"],
        m_samtools_sort = config["mark_duplicates__Picard_MarkDuplicates_samtools_memory"]
    log:
        config["results_dir"]+"/logs/" + config["mark_duplicates__Picard_MarkDuplicates_output_dir"] + "/{sample}_Picard_MarkDuplicates_log.txt"
    threads:
        config["mark_duplicates__Picard_MarkDuplicates_threads"]
    shell:
        "{params.command} "
        "I={input.bam} "
        "O={output.bam} "
        "M={output.metric} "
        "REMOVE_DUPLICATES={params.remove_all_duplicates} "
        "|& tee {log} "
        # Sorting BAM
        "&& samtools sort "
        "-m {params.m_samtools_sort}G "
        "-O BAM "
        "-l 1 "
        "-@ {threads} "
        "-o {output.sorted_bam} "
        "{output.bam} "
        "|& tee -a {log} "
        # Indexing BAM
        "&& samtools index "
        "-@ {threads} "
        "{output.sorted_bam} "
        "|& tee -a {log}"




rule prepare_fasta__gatk_prepare_fasta:
    input:
        **prepare_fasta__gatk_prepare_fasta_inputs(),
    output:
        fasta = config["results_dir"] + "/" + config["prepare_fasta__gatk_prepare_fasta_output_dir"] + "/ref.fa",
        index = config["results_dir"] + "/" + config["prepare_fasta__gatk_prepare_fasta_output_dir"] + "/ref.fa.fai",
        fadict = config["results_dir"] + "/" + config["prepare_fasta__gatk_prepare_fasta_output_dir"] + "/ref.dict"
    log:
        config["results_dir"]+"/logs/" + config["prepare_fasta__gatk_prepare_fasta_output_dir"] + "/gatk_prepare_fasta_log.txt"
    shell:
        "if [[ {input.fasta} =~ \.gz$ ]]; "
        "then gunzip {input.fasta} -c > {output.fasta} ; "
        "else ln -s {input.fasta} {output.fasta}; "
        "fi; "
        "java -jar /opt/biotools/bin/picard.jar CreateSequenceDictionary "
        "R={output.fasta} "
        "O={output.fadict} && "
        "samtools faidx {output.fasta} -o {output.index}"

rule indel_realign__gatk_IndelRealigner_target_creator:
    input:
        **indel_realign__gatk_IndelRealigner_inputs()
    output:
        target_intervals = config["results_dir"]+"/"+config["indel_realign__gatk_IndelRealigner_output_dir"]+"/{sample}_forIndelRealigner.intervals"
    log:
        config["results_dir"]+"/logs/" + config["indel_realign__gatk_IndelRealigner_output_dir"] + "/{sample}_gatk_RealignerTargetCreator_log.txt"
    params:
        command = config["indel_realign__gatk_IndelRealigner_command"]
    shell:
        "{params.command} "
        "-T RealignerTargetCreator "
        "-R {input.genome_fasta} "
        "-I {input.bam} "
        "-o {output.target_intervals} "
        "|& tee {log}"

rule indel_realign__gatk_IndelRealigner:
    input:
        **indel_realign__gatk_IndelRealigner_inputs(),
        target_intervals = config["results_dir"]+"/"+config["indel_realign__gatk_IndelRealigner_output_dir"]+"/{sample}_forIndelRealigner.intervals",
    output:
        bam = temp(config["results_dir"]+"/"+config["indel_realign__gatk_IndelRealigner_output_dir"]+"/{sample}.mapped.dedup.realigned.bam"),
        sorted_bam = config["results_dir"]+"/"+config["indel_realign__gatk_IndelRealigner_output_dir"]+"/{sample}.mapped.dedup.realigned.sorted.bam",
        bai = config["results_dir"]+"/"+config["indel_realign__gatk_IndelRealigner_output_dir"]+"/{sample}.mapped.dedup.realigned.sorted.bam.bai"
    params:
        command = config["indel_realign__gatk_IndelRealigner_command"],
        m_samtools_sort = config["indel_realign__gatk_IndelRealigner_samtools_memory"]
    log:
        config["results_dir"]+"/logs/" + config["indel_realign__gatk_IndelRealigner_output_dir"] + "/{sample}_gatk_IndelRealigner_log.txt"
    threads:
        config["indel_realign__gatk_IndelRealigner_threads"]
    shell:
        "{params.command} "
        "-T IndelRealigner "
        "-R {input.genome_fasta} "
        "-I {input.bam} "
        "-targetIntervals {input.target_intervals} "
        "-o {output.bam} "
        #"-nct {threads} "
        "|& tee {log} "
        # Sorting BAM
        "&& samtools sort "
        "-m {params.m_samtools_sort}G "
        "-O BAM "
        "-l 1 "
        "-@ {threads} "
        "-o {output.sorted_bam} "
        "{output.bam} "
        "|& tee -a {log} "
        # Indexing BAM
        "&& samtools index "
        "-@ {threads} "
        "{output.sorted_bam} "
        "|& tee -a {log}"




rule variant_calling__gatk_haplotype_caller:
    input:
        **variant_calling__gatk_haplotype_caller_inputs()
    output:
        vcf = config["results_dir"]+"/"+config["variant_calling__gatk_haplotype_caller_output_dir"]+"/{sample}_variants.vcf.gz",
        stats = config["results_dir"]+"/"+config["variant_calling__gatk_haplotype_caller_output_dir"]+"/{sample}_vcf_stats.txt"
    threads:
        config["variant_calling__gatk_haplotype_caller_threads"]
    params:
        command = config["variant_calling__gatk_haplotype_caller_command"],
        #bams = list(map("-I {}".format, input.bams)),
        vcf_out = config["results_dir"]+"/"+config["variant_calling__gatk_haplotype_caller_output_dir"]+"/{sample}_variants.vcf",
    log:
        config["results_dir"]+"/logs/" + config["variant_calling__gatk_haplotype_caller_output_dir"] + "/{sample}_gatk_haplotype_caller_log.txt"
    shell:
        "{params.command} "
        "-T HaplotypeCaller "
        "-R {input.genome_fasta} "
        #"{params.bams} "
        "-I {input.bam} "
        "-o {params.vcf_out} "
        "-nct {threads} "
        "|& tee {log} "
        "&& bcftools stats "
        "-F {input.genome_fasta} "
        "-s - "
        "{params.vcf_out} > {output.stats} "
        "|& tee -a {log} ; "
        "bgzip {params.vcf_out} && "
        "tabix -p vcf {output.vcf} "


rule variant_calling__bcftools_mpileup_and_call:
    input:
        **variant_calling__bcftools_mpileup_and_call_inputs()
    output:
        vcf = config["results_dir"]+"/"+config["variant_calling__bcftools_mpileup_and_call_output_dir"]+"/{sample}_variants.vcf.gz",
        stats = config["results_dir"]+"/"+config["variant_calling__bcftools_mpileup_and_call_output_dir"]+"/{sample}_vcf_stats.txt"
    params:
        command = config["variant_calling__bcftools_mpileup_and_call_command"],
        #bams = " ".join(input.bams)
    log:
        config["results_dir"]+"/logs/" + config["variant_calling__bcftools_mpileup_and_call_output_dir"] + "/{sample}_bcftools_mpileup_and_call_log.txt"
    threads:
        config["variant_calling__bcftools_mpileup_and_call_threads"]
    shell:
        "{params.command} mpileup "
        "-Ou " #uncompressed bcf
        "-a FORMAT/AD,FORMAT/DP"
        "-f {input.genome_fasta} "
        #"{params.bams} "
        "{input.bam} "
        "| {params.command} call "
        "-vmO z "
        "-o {output.vcf} "
        "|& tee {log} "
        "&& tabix -p vcf {output.vcf} "
        "|& tee -a {log} "
        "&& {params.command} stats "
        "-F {input.genome_fasta} "
        "-s - "
        "{output.vcf} > {output.stats}"
        "|& tee -a {log} "

rule variant_calling__deep_variant:
    input:
        **variant_calling__deep_variant_inputs()
    output: 
        vcf = config["results_dir"] + "/" + config["variant_calling__deep_variant_output_dir"] + "/{sample}_output.vcf.gz",
        gvcf = config["results_dir"] + "/" + config["variant_calling__deep_variant_output_dir"] + "/{sample}_output.g.vcf.gz",
    params:
        command = config["variant_calling__deep_variant_command"],
        #bam_dir = lambda w, input: os.path.dirname(input.bams[0]),
        output_dir = config["results_dir"] + "/" + config["variant_calling__deep_variant_output_dir"]+ "/",
        out_dir_name = config["variant_calling__deep_variant_output_dir"],
        model_type = config["variant_calling__deep_variant_model_type"]
    log: 
        config["results_dir"] + "/logs/" + config["variant_calling__deep_variant_output_dir"] + "/{sample}_deep_variant_log.txt"
    threads: 
        config["variant_calling__deep_variant_threads"]
    shell:
        "samtools faidx {input.genome_fasta} && "
        "/bin/bash -c \"source /opt/deepvariant/deepvariant_env/bin/activate && "
        "{params.command} "
        "--model_type={params.model_type} "
        "--ref={input.genome_fasta} "
        "--reads={input.bam} "
        "--output_vcf={output.vcf} "
        "--output_gvcf={output.gvcf} "
        "--num_shards={threads} "
        "|& tee {log}\" && "
        # trigger mbb_mqc_plugin to import output.visual_report.html inside multiqc report
        "echo \"1\t./{params.out_dir_name}/output.visual_report.html\" > {params.output_dir}/text.deepvariant.tsv"



rule variant_calling__freebayes:
    input:
        **variant_calling__freebayes_inputs()
    output:
        vcf = config["results_dir"]+"/"+config["variant_calling__freebayes_output_dir"]+"/{sample}_output.vcf.gz",
        #gvcf = config["results_dir"]+"/"+config["variant_calling__freebayes_output_dir"]+"/output.g.vcf",
    params:
        command = config["variant_calling__freebayes_command"],
        #bams = list(map("--bam {}".format, input.bams)),
        vcf_out = config["results_dir"]+"/"+config["variant_calling__freebayes_output_dir"]+"/{sample}_output.vcf",
    log:
        config["results_dir"]+"/logs/" + config["variant_calling__freebayes_output_dir"] + "/{sample}_freebayes_log.txt"
    shell:
        "{params.command} "
        #"{params.bams} "
        "--bam {input.bam} "
        "-f {input.genome_fasta} "
        "--genotype-qualities "
        "> {params.vcf_out} "
        "2> >(tee {log} >&2); "
        "bgzip {params.vcf_out} && "
        "tabix -p vcf {output.vcf} "




import collections

rule prepare_report:
    input:
        *prepare_report_inputs(),
    output:
        *prepare_report_outputs(),
        config_multiqc = config["results_dir"] + "/config_multiqc.yaml",
        params_tab = config["results_dir"] + "/params_tab_mqc.csv",
        versions_tab = config["results_dir"] + "/Tools_version_mqc.csv",
        citations_tab = config["results_dir"] + "/Citations_mqc.csv"
    params:
        params_file = workflow.overwrite_configfiles,
        results_dir = config["results_dir"]
    log:
        config["results_dir"]+"/logs/prepare_report_log.txt"
    run:
        # Specific scripts for each tool
        for script in prepare_report_scripts():
            if (os.path.splitext(script)[1] in [".R",".r"]):
                shell("Rscript "+script+" {params.params_file} |& tee {log}")
            elif (os.path.splitext(script)[1] in [".py"]):
                shell("python3 "+script+" {params.params_file} |& tee {log}")
            elif (os.path.splitext(script)[1] in [".sh"]):
                shell("/bin/bash "+script+" {params.params_file} |& tee {log}")
        # Outputs files for Multiqc report
        outfile = config["results_dir"] + "/outputs_mqc.csv"
        head = """
# description: 'This is the list of the files generated by each step of the workflow'
# section_name: 'Workflow outputs'
"""
        with open(outfile,"w") as out:
            out.write(head)
            out.write("step\ttool\tfile\tdescription\n")#\tname
            for step in STEPS:
                tool = config[step["name"]]
                i=1
                for command in OUTPUTS[step["name"] + "__" + tool]:
                    if ("SeOrPe" not in config.keys() or (config["SeOrPe"] == "SE" and not("_PE" in command)) or (config["SeOrPe"] == "PE" and not("_SE" in command))):
                        outputs = OUTPUTS[step["name"] + "__" + tool][command]
                        for files in outputs:
                            name = files["file"] if 'file' in files.keys() else files["directory"]
                            path = config[command+"_output_dir"] + "/" + name #config["results_dir"] +"/"+
                            out.write(str(i)+"-"+step["title"]+"\t"+tool+"\t"+path+"\t"+files["description"]+"\n")#"\t"+files["name"]+
                            i+=1
        
        # Params list for Multiqc report
        params_list = "params_name\tdescription\tvalue\n"
        head = """# description: 'This is the list of the parameters for each rule'
# section_name: 'Workflow parameters'
"""
        for step in STEPS:
            tool = config[step["name"]]
            for key, value in config.items():
                if (tool in key and tool != "null" and "_command" not in key) or (key in ["results_dir","sample_dir","sample_suffix","SeOrPe"]) and ("SeOrPe" not in config.keys() or (config["SeOrPe"] == "SE" and not("_PE" in key)) or (config["SeOrPe"] == "PE" and not("_SE" in key))):
                    if (key in PARAMS_INFO.keys() and "label" in PARAMS_INFO[key].keys()):
                        description = PARAMS_INFO[key]["label"]
                    else:
                        description = ''
                    params_list += key + "\t'" + description + "'\t'" + str(value) + "'\n"

        with open(output.params_tab,"w") as out:
            out.write(head)
            out.write(params_list)

        # Tools version
        with open(output.versions_tab,"w") as out:
            versions = read_yaml("/workflow/versions.yaml")
            head = """# description: 'This is the list of the tools used and their version'
# section_name: 'Tools version'
"""
            out.write(head)
            out.write("Tool\tVersion"+"\n")
            for tool, version in versions["base_tools"].items():
                out.write(tool+"\t"+str(version)+"\n")
            for step in STEPS:
                tool = config[step["name"]]
                if (tool in versions.keys()):
                    out.write(tool+"\t"+str(versions[tool])+"\n")

        # Citations
        with open(output.citations_tab,"w") as out:
            citations = read_yaml("/workflow/citations.yaml")
            head = """# description: 'This is the list of the citations of used tools'
# section_name: 'Citations'
"""
            out.write(head)
            out.write("Tool\tCitation\n")
            for tool, citation in citations["base_tools"].items():
                out.write(tool+"\t"+" ; ".join(citation)+"\n")
            final_citations = collections.OrderedDict()
            for step in STEPS:
                tool = config[step["name"]]
                if (tool in citations.keys()):
                    final_citations.update(citations[tool])
            for tool, citation in final_citations.items():
                out.write(tool+"\t"+" ; ".join(citation)+"\n")

        # Config for Multiqc report
        shell("python3 /workflow/generate_multiqc_config.py {params.params_file} {output.config_multiqc}")

rule multiqc:
    input:
        multiqc_inputs(),
        config_multiqc = config["results_dir"] + "/config_multiqc.yaml"
    output:
        multiqc_dir = directory(config["results_dir"]+"/multiqc_data"),
        multiqc_report = config["results_dir"]+"/multiqc_report.html"
    params:
        output_dir = config["results_dir"]
    log:
        config["results_dir"]+'/logs/multiqc/multiqc_log.txt'
    run:
        modules_to_run = "-m custom_content "
        for module in MULTIQC.values():
            if (module != "custom"):
                modules_to_run += "-m " + module + " "
        shell(
            "multiqc --config {input.config_multiqc} " +
            "-o {params.output_dir} " +
            "-f {params.output_dir} " +
            modules_to_run +
            "|& tee {log}"
        )

# Final Snakemake rule waiting for outputs of the final step choosen by user (default all steps)
rule all:
    input:
        workflow_outputs("all")
    output:
        Snakefile = config["results_dir"]+"/workflow/Snakefile",
        scripts = directory(config["results_dir"]+"/workflow/scripts"),
        params = config["results_dir"]+"/workflow/params.yml"
    params:
        params_file = workflow.overwrite_configfiles,
    shell:
        "cp /workflow/Snakefile {output.Snakefile} && "
        "cp -r /workflow/scripts {output.scripts} && "
        "cp {params.params_file} {output.params}"

onsuccess:
    print("Workflow finished with SUCCESS")
    shell("touch "+config["results_dir"]+"/logs/workflow_end.ok")

onerror:
    print("An ERROR occurred")
    shell("cat {log} > "+config["results_dir"]+"/logs/workflow_end.error")
    #shell("mail -s "an error occurred" youremail@provider.com < {log}")