save_params <- function(path_param){
	res = ""	# 	Page : global_params
		res = paste0(res , paste("global_params:", paste0('"', input$selectglobal_params, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$results_dir))) {
			res = paste0(res, paste("results_dir:", format(input$results_dir, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("results_dir:", paste0('"', input$results_dir, '"'), "\n", sep = " "))
		}	

		res = paste0(res, paste("genome_fasta_select:", paste0('"', input$genome_fasta_select, '"'), "\n", sep = " "))
		if(input$genome_fasta_select == "server") {
			res = paste0(res, paste("genome_fasta:", paste0('"', input$genome_fasta_server, '"'), "\n", sep = " "))
		}
		if(input$genome_fasta_select == "local"){
			res = paste0(res, paste("genome_fasta:", paste0('"', input$genome_fasta_local$datapath, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$sample_dir))) {
			res = paste0(res, paste("sample_dir:", format(input$sample_dir, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("sample_dir:", paste0('"', input$sample_dir, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$SeOrPe))) {
			res = paste0(res, paste("SeOrPe:", input$SeOrPe, "\n", sep = " "))
		} else {
			res = paste0(res, paste("SeOrPe:", paste0('"', input$SeOrPe, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$memo))) {
			res = paste0(res, paste("memo:", format(input$memo, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("memo:", paste0('"', input$memo, '"'), "\n", sep = " "))
		}	

	# 	Page : preprocess
		res = paste0(res , paste("preprocess:", paste0('"', input$selectpreprocess, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$preprocess__fastp_threads))) {
			res = paste0(res, paste("preprocess__fastp_threads:", format(input$preprocess__fastp_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_threads:", paste0('"', input$preprocess__fastp_threads, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$preprocess__fastp_complexity_threshold))) {
			res = paste0(res, paste("preprocess__fastp_complexity_threshold:", format(input$preprocess__fastp_complexity_threshold, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_complexity_threshold:", paste0('"', input$preprocess__fastp_complexity_threshold, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$preprocess__fastp_report_title))) {
			res = paste0(res, paste("preprocess__fastp_report_title:", format(input$preprocess__fastp_report_title, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_report_title:", paste0('"', input$preprocess__fastp_report_title, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$preprocess__fastp_adapter_sequence))) {
			res = paste0(res, paste("preprocess__fastp_adapter_sequence:", format(input$preprocess__fastp_adapter_sequence, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_adapter_sequence:", paste0('"', input$preprocess__fastp_adapter_sequence, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$preprocess__fastp_adapter_sequence_R2_PE))) {
			res = paste0(res, paste("preprocess__fastp_adapter_sequence_R2_PE:", format(input$preprocess__fastp_adapter_sequence_R2_PE, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_adapter_sequence_R2_PE:", paste0('"', input$preprocess__fastp_adapter_sequence_R2_PE, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$preprocess__fastp_P))) {
			res = paste0(res, paste("preprocess__fastp_P:", format(input$preprocess__fastp_P, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_P:", paste0('"', input$preprocess__fastp_P, '"'), "\n", sep = " "))
		}	

		if(input$preprocess__fastp_correction_PE) {
			res = paste0(res, paste("preprocess__fastp_correction_PE:", "true", "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_correction_PE:", "false", "\n", sep = " "))
		}	

		if(input$preprocess__fastp_low_complexity_filter) {
			res = paste0(res, paste("preprocess__fastp_low_complexity_filter:", "true", "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_low_complexity_filter:", "false", "\n", sep = " "))
		}	

		if(input$preprocess__fastp_overrepresentation_analysis) {
			res = paste0(res, paste("preprocess__fastp_overrepresentation_analysis:", "true", "\n", sep = " "))
		} else {
			res = paste0(res, paste("preprocess__fastp_overrepresentation_analysis:", "false", "\n", sep = " "))
		}	

	# 	Page : mapping
		res = paste0(res , paste("mapping:", paste0('"', input$selectmapping, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$mapping__bwa_index_path))) {
			res = paste0(res, paste("mapping__bwa_index_path:", format(input$mapping__bwa_index_path, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bwa_index_path:", paste0('"', input$mapping__bwa_index_path, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bwa_index_algorithm))) {
			res = paste0(res, paste("mapping__bwa_index_algorithm:", input$mapping__bwa_index_algorithm, "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bwa_index_algorithm:", paste0('"', input$mapping__bwa_index_algorithm, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bwa_mem_threads))) {
			res = paste0(res, paste("mapping__bwa_mem_threads:", format(input$mapping__bwa_mem_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bwa_mem_threads:", paste0('"', input$mapping__bwa_mem_threads, '"'), "\n", sep = " "))
		}	

		if(input$mapping__bwa_mem_quality0_multimapping) {
			res = paste0(res, paste("mapping__bwa_mem_quality0_multimapping:", "true", "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bwa_mem_quality0_multimapping:", "false", "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bowtie_index_path))) {
			res = paste0(res, paste("mapping__bowtie_index_path:", format(input$mapping__bowtie_index_path, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_index_path:", paste0('"', input$mapping__bowtie_index_path, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bowtie_index_threads))) {
			res = paste0(res, paste("mapping__bowtie_index_threads:", format(input$mapping__bowtie_index_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_index_threads:", paste0('"', input$mapping__bowtie_index_threads, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bowtie_threads))) {
			res = paste0(res, paste("mapping__bowtie_threads:", format(input$mapping__bowtie_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_threads:", paste0('"', input$mapping__bowtie_threads, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bowtie_minins_PE))) {
			res = paste0(res, paste("mapping__bowtie_minins_PE:", format(input$mapping__bowtie_minins_PE, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_minins_PE:", paste0('"', input$mapping__bowtie_minins_PE, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bowtie_maxins_PE))) {
			res = paste0(res, paste("mapping__bowtie_maxins_PE:", format(input$mapping__bowtie_maxins_PE, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_maxins_PE:", paste0('"', input$mapping__bowtie_maxins_PE, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bowtie_orientation_PE))) {
			res = paste0(res, paste("mapping__bowtie_orientation_PE:", input$mapping__bowtie_orientation_PE, "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_orientation_PE:", paste0('"', input$mapping__bowtie_orientation_PE, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mapping__bowtie_mult_align_limit))) {
			res = paste0(res, paste("mapping__bowtie_mult_align_limit:", format(input$mapping__bowtie_mult_align_limit, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_mult_align_limit:", paste0('"', input$mapping__bowtie_mult_align_limit, '"'), "\n", sep = " "))
		}	

		if(input$mapping__bowtie_best) {
			res = paste0(res, paste("mapping__bowtie_best:", "true", "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_best:", "false", "\n", sep = " "))
		}	

		if(input$mapping__bowtie_strata) {
			res = paste0(res, paste("mapping__bowtie_strata:", "true", "\n", sep = " "))
		} else {
			res = paste0(res, paste("mapping__bowtie_strata:", "false", "\n", sep = " "))
		}	

	# 	Page : mark_duplicates
		res = paste0(res , paste("mark_duplicates:", paste0('"', input$selectmark_duplicates, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$mark_duplicates__Picard_MarkDuplicates_threads))) {
			res = paste0(res, paste("mark_duplicates__Picard_MarkDuplicates_threads:", format(input$mark_duplicates__Picard_MarkDuplicates_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mark_duplicates__Picard_MarkDuplicates_threads:", paste0('"', input$mark_duplicates__Picard_MarkDuplicates_threads, '"'), "\n", sep = " "))
		}	

		if(input$mark_duplicates__Picard_MarkDuplicates_remove_all_duplicates) {
			res = paste0(res, paste("mark_duplicates__Picard_MarkDuplicates_remove_all_duplicates:", "true", "\n", sep = " "))
		} else {
			res = paste0(res, paste("mark_duplicates__Picard_MarkDuplicates_remove_all_duplicates:", "false", "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$mark_duplicates__Picard_MarkDuplicates_samtools_memory))) {
			res = paste0(res, paste("mark_duplicates__Picard_MarkDuplicates_samtools_memory:", format(input$mark_duplicates__Picard_MarkDuplicates_samtools_memory, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("mark_duplicates__Picard_MarkDuplicates_samtools_memory:", paste0('"', input$mark_duplicates__Picard_MarkDuplicates_samtools_memory, '"'), "\n", sep = " "))
		}	

	# 	Page : prepare_fasta
		res = paste0(res , paste("prepare_fasta:", paste0('"', input$selectprepare_fasta, '"'), "\n", sep = " "))
	# 	Page : indel_realign
		res = paste0(res , paste("indel_realign:", paste0('"', input$selectindel_realign, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$indel_realign__gatk_IndelRealigner_threads))) {
			res = paste0(res, paste("indel_realign__gatk_IndelRealigner_threads:", format(input$indel_realign__gatk_IndelRealigner_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("indel_realign__gatk_IndelRealigner_threads:", paste0('"', input$indel_realign__gatk_IndelRealigner_threads, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$indel_realign__gatk_IndelRealigner_samtools_memory))) {
			res = paste0(res, paste("indel_realign__gatk_IndelRealigner_samtools_memory:", format(input$indel_realign__gatk_IndelRealigner_samtools_memory, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("indel_realign__gatk_IndelRealigner_samtools_memory:", paste0('"', input$indel_realign__gatk_IndelRealigner_samtools_memory, '"'), "\n", sep = " "))
		}	

	# 	Page : variant_calling
		res = paste0(res , paste("variant_calling:", paste0('"', input$selectvariant_calling, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$variant_calling__gatk_haplotype_caller_threads))) {
			res = paste0(res, paste("variant_calling__gatk_haplotype_caller_threads:", format(input$variant_calling__gatk_haplotype_caller_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("variant_calling__gatk_haplotype_caller_threads:", paste0('"', input$variant_calling__gatk_haplotype_caller_threads, '"'), "\n", sep = " "))
		}	

		res = paste0(res, paste("variant_calling__gatk_haplotype_caller_genome_fasta_select:", paste0('"', input$variant_calling__gatk_haplotype_caller_genome_fasta_select, '"'), "\n", sep = " "))
		if(input$variant_calling__gatk_haplotype_caller_genome_fasta_select == "server") {
			res = paste0(res, paste("variant_calling__gatk_haplotype_caller_genome_fasta:", paste0('"', input$variant_calling__gatk_haplotype_caller_genome_fasta_server, '"'), "\n", sep = " "))
		}
		if(input$variant_calling__gatk_haplotype_caller_genome_fasta_select == "local"){
			res = paste0(res, paste("variant_calling__gatk_haplotype_caller_genome_fasta:", paste0('"', input$variant_calling__gatk_haplotype_caller_genome_fasta_local$datapath, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$variant_calling__bcftools_mpileup_and_call_threads))) {
			res = paste0(res, paste("variant_calling__bcftools_mpileup_and_call_threads:", format(input$variant_calling__bcftools_mpileup_and_call_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("variant_calling__bcftools_mpileup_and_call_threads:", paste0('"', input$variant_calling__bcftools_mpileup_and_call_threads, '"'), "\n", sep = " "))
		}	

		res = paste0(res, paste("variant_calling__bcftools_mpileup_and_call_genome_fasta_select:", paste0('"', input$variant_calling__bcftools_mpileup_and_call_genome_fasta_select, '"'), "\n", sep = " "))
		if(input$variant_calling__bcftools_mpileup_and_call_genome_fasta_select == "server") {
			res = paste0(res, paste("variant_calling__bcftools_mpileup_and_call_genome_fasta:", paste0('"', input$variant_calling__bcftools_mpileup_and_call_genome_fasta_server, '"'), "\n", sep = " "))
		}
		if(input$variant_calling__bcftools_mpileup_and_call_genome_fasta_select == "local"){
			res = paste0(res, paste("variant_calling__bcftools_mpileup_and_call_genome_fasta:", paste0('"', input$variant_calling__bcftools_mpileup_and_call_genome_fasta_local$datapath, '"'), "\n", sep = " "))
		}	

		res = paste0(res, paste("variant_calling__deep_variant_genome_fasta_select:", paste0('"', input$variant_calling__deep_variant_genome_fasta_select, '"'), "\n", sep = " "))
		if(input$variant_calling__deep_variant_genome_fasta_select == "server") {
			res = paste0(res, paste("variant_calling__deep_variant_genome_fasta:", paste0('"', input$variant_calling__deep_variant_genome_fasta_server, '"'), "\n", sep = " "))
		}
		if(input$variant_calling__deep_variant_genome_fasta_select == "local"){
			res = paste0(res, paste("variant_calling__deep_variant_genome_fasta:", paste0('"', input$variant_calling__deep_variant_genome_fasta_local$datapath, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$variant_calling__deep_variant_threads))) {
			res = paste0(res, paste("variant_calling__deep_variant_threads:", format(input$variant_calling__deep_variant_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("variant_calling__deep_variant_threads:", paste0('"', input$variant_calling__deep_variant_threads, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$variant_calling__deep_variant_model_type))) {
			res = paste0(res, paste("variant_calling__deep_variant_model_type:", input$variant_calling__deep_variant_model_type, "\n", sep = " "))
		} else {
			res = paste0(res, paste("variant_calling__deep_variant_model_type:", paste0('"', input$variant_calling__deep_variant_model_type, '"'), "\n", sep = " "))
		}	

		res = paste0(res, paste("variant_calling__freebayes_genome_fasta_select:", paste0('"', input$variant_calling__freebayes_genome_fasta_select, '"'), "\n", sep = " "))
		if(input$variant_calling__freebayes_genome_fasta_select == "server") {
			res = paste0(res, paste("variant_calling__freebayes_genome_fasta:", paste0('"', input$variant_calling__freebayes_genome_fasta_server, '"'), "\n", sep = " "))
		}
		if(input$variant_calling__freebayes_genome_fasta_select == "local"){
			res = paste0(res, paste("variant_calling__freebayes_genome_fasta:", paste0('"', input$variant_calling__freebayes_genome_fasta_local$datapath, '"'), "\n", sep = " "))
		}	

	a = yaml.load_file("/workflow/params.total.yml", handlers=list("float#fix"=function(x){ format(x,scientific=F)}))
	p = a[["params"]]
	a["params"] = NULL
	b = yaml.load(res, handlers=list("float#fix"=function(x){ format(x,scientific=F)}))
	pnotb = subset(names(p), !(names(p)%in%names(b)))
	d = list()
	d$params = c(p[pnotb],b)
	logical = function(x) {
		result <- ifelse(x, "True", "False")
		class(result) <- "verbatim"
		return(result)
	}
	d = c(d,a)
	write_yaml(d,path_param,handlers=list(logical = logical,"float#fix"=function(x){ format(x,scientific=F) }))
	}

force_rule <- function(force_from){
	if (input$force_from=="none"){
		return("")
	}
	else if (input$force_from=="all"){ return("--forcerun all") }
	else {
		params = yaml.load_file(paste0(input$results_dir,"/params.yml"), handlers=list("float#fix"=function(x){ format(x,scientific=F)}))
		outputs = params[["outputs"]]
		tool = params[["params"]][[force_from]]
		steptool = paste0(force_from,"__",tool)
		if (length(outputs[[steptool]])==1)
			rule = names(outputs[[steptool]])[[1]]
		else{
			rule = names(outputs[[steptool]])[[grep(input$SeOrPe,names(outputs[[steptool]]))]]
		}
		return(paste0("--forcerun ",rule))
	}
}
#' Event when use RULEGRAPH button
observeEvent({c(input$sidebarmenu,input$refresh_rg)}, {

	if (!dir.exists(paste0(input$results_dir,"/logs"))){
		dir.create(paste0(input$results_dir,"/logs"))
	}
	if(input$sidebarmenu=="RULEGRAPH"){
		input_list <- reactiveValuesToList(input)
		toggle_inputs(input_list,F,F)
		path_param <- paste0(input$results_dir,"/params.yml")

		save_params(path_param)
		i = sample.int(1000,size = 1)

		system(paste0("rm ",input$results_dir,"/rulegraph*"))

		outUI = tryCatch({
			system(paste0("snakemake -s /workflow/Snakefile --configfile ",input$results_dir,"/params.yml -d ",input$results_dir," all --rulegraph 1> ",input$results_dir,"/rulegraph",i,".dot 2> ",input$results_dir,"/logs/rulegraph.txt"),intern=T)
			system(paste0("cat ",input$results_dir,"/rulegraph",i,".dot | dot -Tsvg -Gratio=0.75 > ",input$results_dir,"/rulegraph",i,".svg"),intern=T)
			tagList(img(src = paste0("results/rulegraph",i,".svg") ,alt = "Rulegraph of Snakemake jobs",style="max-width: 100%;height: auto;display: block;margin: auto"))},
		error = function(e){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(HTML(paste(readLines(paste0(input$results_dir,"/logs/rulegraph.txt"),warn=F), collapse = "<br/>"))))},
		warning = function(w){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(HTML(paste(readLines(paste0(input$results_dir,"/logs/rulegraph.txt"),warn=F), collapse = "<br/>"))))})
		addResourcePath("results", input$results_dir)
		output$RULEGRAPH_svg = renderUI(outUI)
		toggle_inputs(input_list,T,F)
}})
#' Event when use RunPipeline button
observeEvent(input$RunPipeline, {

	rv$running = T
	input_list <- reactiveValuesToList(input)
	toggle_inputs(input_list,F,F)
	updateTabsetPanel(session, "sidebarmenu", selected = "run_out")
	path_param <- paste0(input$results_dir,"/params.yml")

		save_params(path_param)


	outUI = tryCatch({
		if (!dir.exists(paste0(input$results_dir,"/logs"))){
			dir.create(paste0(input$results_dir,"/logs"))
		}
		if (!file.exists(paste0(input$results_dir,"/logs/runlog.txt"))){
			file.create(paste0(input$results_dir,"/logs/runlog.txt"))
		}
		system(paste0("touch ",input$results_dir,"/logs/workflow.running"),wait = T)
			system(paste0("snakemake -s /workflow/Snakefile --configfile ",input$results_dir,"/params.yml -d ",input$results_dir," all --rulegraph | dot -Tpng -Gratio=0.75 > ",input$results_dir,"/Rule_graph_mqc.png"))
		force = force_rule(input$force_from)
		rerun = if (input$rerun_incomplete) "--rerun-incomplete" else ""
		system2("python3",paste0("-u -m snakemake -s /workflow/Snakefile --configfile ", paste0(input$results_dir,"/params.yml") ,	" -d ", input$results_dir ,	" --cores ", input$cores, " all ", force, " ",rerun),wait = FALSE, stdout = paste0(input$results_dir,"/logs/runlog.txt"), stderr = paste0(input$results_dir,"/logs/runlog.txt"))
		tags$iframe(src="results/multiqc_report.html",width="100%", height="900px")},
		error = function(e){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(paste0("error : ",e$message)))},
		warning = function(w){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(paste0("error : ",w$message)))})
		})

		shinyDirChoose(input, "shinydir_results_dir", root=c(Results="/Results"),session = session)
		observeEvent({parseDirPath(c(Results="/Results"),input$shinydir_results_dir)},{
			updateTextInput(session,"results_dir",value = parseDirPath(c(Results="/Results"),input$shinydir_results_dir))
		})

		shinyFileChoose(input, "shinyfiles_genome_fasta", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_genome_fasta)$datapath[1]},{
			if(length(parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_genome_fasta)$datapath)==1){
				updateTextInput(session,"genome_fasta_server",value = parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_genome_fasta)$datapath[[1]])
			}
		})

		shinyDirChoose(input, "shinydir_sample_dir", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_sample_dir)},{
			updateTextInput(session,"sample_dir",value = parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_sample_dir))
		})

		shinyDirChoose(input, "shinydir_mapping__bwa_index_path", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_mapping__bwa_index_path)},{
			updateTextInput(session,"mapping__bwa_index_path",value = parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_mapping__bwa_index_path))
		})

		shinyDirChoose(input, "shinydir_mapping__bowtie_index_path", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_mapping__bowtie_index_path)},{
			updateTextInput(session,"mapping__bowtie_index_path",value = parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_mapping__bowtie_index_path))
		})

		shinyFileChoose(input, "shinyfiles_variant_calling__gatk_haplotype_caller_genome_fasta", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__gatk_haplotype_caller_genome_fasta)$datapath[1]},{
			if(length(parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__gatk_haplotype_caller_genome_fasta)$datapath)==1){
				updateTextInput(session,"variant_calling__gatk_haplotype_caller_genome_fasta_server",value = parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__gatk_haplotype_caller_genome_fasta)$datapath[[1]])
			}
		})

		shinyFileChoose(input, "shinyfiles_variant_calling__bcftools_mpileup_and_call_genome_fasta", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__bcftools_mpileup_and_call_genome_fasta)$datapath[1]},{
			if(length(parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__bcftools_mpileup_and_call_genome_fasta)$datapath)==1){
				updateTextInput(session,"variant_calling__bcftools_mpileup_and_call_genome_fasta_server",value = parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__bcftools_mpileup_and_call_genome_fasta)$datapath[[1]])
			}
		})

		shinyFileChoose(input, "shinyfiles_variant_calling__deep_variant_genome_fasta", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__deep_variant_genome_fasta)$datapath[1]},{
			if(length(parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__deep_variant_genome_fasta)$datapath)==1){
				updateTextInput(session,"variant_calling__deep_variant_genome_fasta_server",value = parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__deep_variant_genome_fasta)$datapath[[1]])
			}
		})

		shinyFileChoose(input, "shinyfiles_variant_calling__freebayes_genome_fasta", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__freebayes_genome_fasta)$datapath[1]},{
			if(length(parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__freebayes_genome_fasta)$datapath)==1){
				updateTextInput(session,"variant_calling__freebayes_genome_fasta_server",value = parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_variant_calling__freebayes_genome_fasta)$datapath[[1]])
			}
		})


